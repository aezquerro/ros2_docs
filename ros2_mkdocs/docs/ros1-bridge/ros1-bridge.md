# Setup of ROS1 Bridge

**Ros1-Bridge** connects messages from ROS1 and ROS2. The prebuilt version supports some messages, and most of the ROS core messages used.

# Prerequisites

In order to be able to use the ROS1 Bridge, you need to have ROS1 installed in your machine. Since this a ROS2 documentation site, we are not going to cover this steps. You can follow ROS1 installation steps in the official documentation, here:  

[ROS1 Installation Guide](http://wiki.ros.org/ROS/Installation)

## ROS1-Bridge sourcing

So, for this first example, we are going to just reproduce the standard tests for ROS1-Bridge. This way, you will get the hang of how this system works.

The first thing to do is to create a custom bridge bashrc configuration to be able to source both the ROS1 and ROS2 systems that ROS1-Bridge needs. So, create a new file named **.bashrc_bridge** inside your user folder (**/home/user/**) that contains the following:

```
source /opt/ros/melodic/setup.bash
source /opt/ros/dashing/local_setup.bash
```

Create the file and paste the above contents:

```
cd
touch .bashrc_bridge
```

ROS1-Bridge is based on several concepts, but the most basic one is the fact that the shell where you launch **ROS1-Bridge** has to have sourced, all the paths to all the messages that it has access to. This means that it has to be able to reach both ROS1 and ROS2 message definitions. That's why both **melodic** and **dashing** are sourced, to be able to reach ROS1 and ROS2 system installed packages. And also, it has to be able to reach any workspace that you might use. In this case, the **catkin_ws** for ROS1 and **ros2_ws** for ROS2.

 If you want to know in more depth how this correspondance is done, please check out the official documentation here: https://github.com/ros2/ros1_bridge/blob/master/doc/index.rst

## Basic Examples

Here we will perform the basic classical ROS1-Bridge example tests, to get the hang of ROS1-Bridge.

**Important Note**: Please start from fresh Shells to avoid any past sourcing.

We will use **two extra bashrc files**, one for ROS1 and another one for ROS2. 

Create a new file named **.bashrc_ros1**:

```
cd
touch .bashrc_ros1
```

Paste the following contents to this new file:

```
# ROS1
export ROS_DISTRO=melodic
source /opt/ros/$ROS_DISTRO/setup.bash
source /home/user/catkin_ws/devel/setup.bash
```

Create a new file named **.bashrc_ros2**:

```
cd
touch .bashrc_ros2
```

Paste the following contents to this new file:

```
# ROS2
export ROS_DISTRO=dashing
source /opt/ros/$ROS_DISTRO/setup.bash
source /home/user/ros2_ws/install/local_setup.bash
```



### Example 1a: Run the bridge and the example talker and listener ROS1->ROS2

For this, you will need **three** different Shells, which we will name (for future reference):

- Shell 1: ROS1-Bridge
- Shell 2: Talker ROS1
- Shell 3: Listener ROS2

**NOTE:** In Robot Ignite Academy, you already have a **roscore** running, just because you have gazebo for ROS1 in the simulations that use ROS1, so you don't need to launch it. But in an empty local computer, you would need to do it, sourcing **ROS1** paths.

In Shell 1, source the Bridge file and launch the **dynamic_bridge** node:

```
source /home/user/.bashrc_bridge
export ROS_MASTER_URI=http://localhost:11311
ros2 run ros1_bridge dynamic_bridge
```



In Shell 2, we source ROS1 and launch the **talker** node in ROS1:

```
source /home/user/.bashrc_ros1
rosrun rospy_tutorials talker
```

In Shell 3, we source ROS2 and launch the **listener** node in ROS2:

```
source /home/user/.bashrc_ros2
ros2 run demo_nodes_cpp listener
```

Shell #1 Output: ROS1-Bridge

```
created 1to2 bridge for topic '/chatter' with ROS 1 type 'std_msgs/String' and ROS 2 type 'std_msgs/String'
[INFO] [ros1_bridge]: Passing message from ROS 1 std_msgs/String to ROS 2 std_msgs/String (showing msg only once per type)
removed 1to2 bridge for topic '/chatter'
```

Shell #2 Output: Talker ROS1

```
[INFO] [1544551384.459151]: hello world 1544551384.46
[INFO] [1544551384.559150]: hello world 1544551384.56
[INFO] [1544551384.659144]: hello world 1544551384.66
```

Shell #3 Output: Listener ROS2

```
[INFO] [listener]: I heard: [hello world 1544551383.66]
[INFO] [listener]: I heard: [hello world 1544551383.76]
[INFO] [listener]: I heard: [hello world 1544551383.86]
```

### Example 1b: Run the bridge and the example talker and listener ROS2->ROS1

For this you will need **three** different Shells, which we will name (for future reference):

- Shell 1: ROS1-Bridge
- Shell 2: Listener ROS1
- Shell 3: Talker ROS2

In Shell 1, source the Bridge file and launch the **dynamic_bridge** node:

```
source /home/user/.bashrc_bridge
export ROS_MASTER_URI=http://localhost:11311
ros2 run ros1_bridge dynamic_bridge
```

In Shell 2, we source ROS1 and launch the **listener** node in ROS1:

```
source /home/user/.bashrc_ros1
rosrun roscpp_tutorials listener
```

In Shell 3, we source ROS2 and launch the **talker** node in ROS2:

```
source /home/user/.bashrc_ros2
ros2 run demo_nodes_py talker
```

WebShell #1 Output: ROS1-Bridge

```
created 2to1 bridge for topic '/chatter' with ROS 2 type 'std_msgs/String' and ROS 1 type ''
removed 2to1 bridge for topic '/chatter'
```

WebShell #2 Output: Listener ROS1

```
[ INFO] [1544551475.461572300]: I heard: [Hello World: 0]
[ INFO] [1544551476.453461509]: I heard: [Hello World: 1]
[ INFO] [1544551477.453674267]: I heard: [Hello World: 2]
```

WebShell #3 Output: Talker ROS2

```
[INFO] [talker]: Publishing: "Hello World: 0"
[INFO] [talker]: Publishing: "Hello World: 1"
[INFO] [talker]: Publishing: "Hello World: 2"
```

### Example 2: Run the bridge for AddTwoInts service

For this you will need **four** different webshells, which we will name (for future reference):

- WebShell 1: ROS1-Bridge
- WebShell 2: add_two_ints_server ROS1
- WebShell 3: add_two_ints_client ROS2

In Shell 1, source the Bridge file and launch the **dynamic_bridge** node:

```
# .bashrc_bridge sources everything and was used for the compilation 
. /home/user/.bashrc_bridge
export ROS_MASTER_URI=http://localhost:11311
ros2 run ros1_bridge dynamic_bridge
```

In Shell 2, we source ROS1 and launch the **server** node in ROS1:

```
# .bashrc_bridge sources everything and was used for the compilation 
. /home/user/.bashrc_ros1
export ROS_MASTER_URI=http://localhost:11311
rosrun roscpp_tutorials add_two_ints_server
```

In Shell 3, we source ROS2 and launch the **client** node in ROS2:

```
# .bashrc_bridge sources everything and was used for the compilation 
. /home/user/.bashrc_ros2
ros2 run demo_nodes_cpp add_two_ints_client
```

WebShell #1 Output: ROS1-Bridge

```
Created 2 to 1 bridge for service /add_two_ints
Removed 2 to 1 bridge for service /add_two_ints
```

WebShell #2 Output: add_two_ints_server ROS1

```
[ INFO] [1544551634.781450042]: request: x=2, y=3
[ INFO] [1544551634.782151118]:   sending back response: [5]
```

WebShell #3 Output: add_two_ints_client ROS2

```
[INFO] [add_two_ints_client]: Result of add_two_ints: 
```

## Practice Online

Also, you can test this tutorial in ROSDS, using a ROSject which already contains all the code described in it. You can get the ROSject by clicking on the button below:

<a href="http://www.rosject.io/l/e102032/" target="_blank"><img src="../img/rosject.png" width="35%"></a>

*Above: Get ROSDS ROSject*

The link above will take yo a page like the below one:

<img src="../img/custom-msg-service-rosject.png">

Now, you just need to Sign In (or Sign Up if you don't have an account yet) to ROSDS in order to launch the ROSject.


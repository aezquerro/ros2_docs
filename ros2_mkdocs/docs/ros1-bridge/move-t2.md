# Move a Turtlebot 2 robot with ROS1 Bridge and ROS2

In this tutorial, you are going to do the following:

* First, you will launch a Turtlebot2 simulation that runs in ROS1.
* Second, you will launch ROS1 Bridge in order to be able to communicate with ROS1 topics and services from ROS2.
* Finally, you will create a program in ROS2 that will move the Turtlebot 2 robot.

# Prerequisites

In order to be able to use the ROS1 Bridge, and to launch the Turtlebot2 simulation in ROS1, you need to have ROS1 installed in your machine, of course. Since this a ROS2 documentation site, we are not going to cover this steps. You can follow ROS1 installation steps in the official documentation, here:  

[ROS1 Installation Guide](http://wiki.ros.org/ROS/Installation)

## Launch Turtlebot2 simulation in ROS1

First of all, you will need to get the Turtlebot2 simulation packages. You can get them from The Construct Public Simulations repository: 

    source /opt/ros/melodic/setup.bash
    mkdir -p ~/catkin_ws/src
    cd ~/catkin_ws
    catkin_make

Now, execute the following command to compile the packages:

    cd ~/catkin_ws/src
    git clone https://aezquerro@bitbucket.org/theconstructcore/turtlebot.git
    cd ~/catkin_ws
    catkin_make
    source ~/catkin_ws/devel/setup.bash

Now, you can start the simulation with the following command:

    roslaunch turtlebot_gazebo main.launch

You will get an output similar to this one:

<img src="../img/sim_turtle.png">



## Set Up ROS1 Bridge

Now, you will need to set up the ROS1 Bridge.:

    source /opt/ros/melodic/setup.bash
    source /opt/ros/dashing/setup.bash

Now, execute the following command to launch the dynamic bridge:

    ros2 run ros1_bridge dynamic_bridge

You will get an output similar to this one:

<img src="../img/create-pkg-publisher.png">



## Create your ROS2 program

First of all, you will have to create a new package, where you will place the code for your Publisher node. For this, you need to be in your ROS2 workspace `src` folder.

    cd ~/ros2_ws/src

Now, execute the following command to create the package:

    ros2 pkg create --build-type ament_cmake move_turtlebot --dependencies rclcpp geometry_msgs

You will get an output similar to this one:

<img src="../img/create-pkg-publisher.png">

This will automatically generate your package and all its necessary files and folders. Now, navigate to the 'ros2_ws/src/' folder in order to create your file.

## Write the code

Inside the `ros2_ws/src` folder, create a new file named move_t2.cpp`. Into this file, you will copy the following code

    #include "geometry_msgs/msg/twist.hpp"
    #include "rclcpp/rclcpp.hpp"
    #include <chrono>
    
    using namespace std::chrono_literals;
    
    /* This example creates a subclass of Node and uses std::bind() to register a
     * member function as a callback from the timer. */
    
    class SimplePublisher : public rclcpp::Node {
    public:
      SimplePublisher() : Node("simple_publisher") {
        publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("/cmd_vel");
        timer_ = this->create_wall_timer(
            500ms, std::bind(&SimplePublisher::timer_callback, this));
      }
    
    private:
      void timer_callback() {
        auto message = geometry_msgs::msg::Twist();
        message.linear.x = 0.5;
        message.angular.z = 0.5;
        publisher_->publish(message);
      }
      rclcpp::TimerBase::SharedPtr timer_;
      rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
    };
    
    int main(int argc, char *argv[]) {
      rclcpp::init(argc, argv);
      rclcpp::spin(std::make_shared<SimplePublisher>());
      rclcpp::shutdown();
      return 0;
    }

## Review the code

As you can see, the code is pretty similar as the example Publisher you've already seen in this tutorials. One of the differences, for instance, is that we are now importing a Twist message.

    #include "geometry_msgs/msg/twist.hpp"

The Twist message is used for sending velocity commands to a wheeled robot.

Also, note that we are now subscribing to the `/cmd_vel` topic, which is the topic that received the velocity commands in this simulation:

    publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("/cmd_vel");

Finally, inside the `timer_callback()` function, we fill the Twist message with the desired values in order to move the robot, and we publish this message into the topic.

    void timer_callback() {
        auto message = geometry_msgs::msg::Twist();
        message.linear.x = 0.5;
        message.angular.z = 0.5;
        publisher_->publish(message);
      }




## Modify the CMakeLists.txt file

In the Build section of your CMakeLists.txt file, add the following lines to your CMakeLists.txt file, right above the ament_package() line.

    add_executable(publisher_node src/publisher.cpp)
    ament_target_dependencies(publisher_node rclcpp std_msgs)
    
    install(TARGETS
    publisher_node
    DESTINATION lib/${PROJECT_NAME}
    )


## Compile the package

First of all, let's go the ros2_ws folder in order to be able to compile.

    cd ~/ros2_ws;

Let's now execute the colcon command in order to build our node.

    colcon build --symlink-install

You will get an output like the following one:

<img src="../img/publisher-compilation.png">

Finally, let's source the workspace.

    source ~/ros2_ws/install/setup.bash

## Move the robot!

Let's run our programwith the following command:

    ros2 run move_turtlebot move_turtlebot_node

You will now see how the robot starts moving, like this:

<img src="../img/kobuki_circle.gif">

## Watch a Video Demonstration!

Also, you can test this tutorial in ROSDS, using a ROSject which already contains all the code described in it. You can get the ROSject by clicking on the button below:

The link above will take yo a page like the below one:

 [![Click to Open Video](img/movet2_thumbnail2.png)]( https://www.youtube.com/watch?v=RW8aZ_0D2UI ) 

## Practice Online

Also, you can test this tutorial in ROSDS, using a ROSject which already contains all the code described in it. You can get the ROSject by clicking on the button below:

<a href="http://www.rosject.io/l/e2db617/" target="_blank"><img src="../img/rosject.png"  width="35%"></a>

*Above: Get ROSDS ROSject*

The link above will take yo a page like the below one:

<img src="../img/movet2_rosject.png">

Now, you just need to Sign In (or Sign Up if you don't have an account yet) to ROSDS in order to launch the ROSject.
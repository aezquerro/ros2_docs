# Installing ROS2 via Debian Packages

## Setup Locale
Make sure you have a locale which supports UTF-8. If you are in a minimal environment, such as a docker container, the locale may be something minimal like POSIX. We test with the following settings. It should be fine if you’re using a different UTF-8 supported locale.

    sudo locale-gen en_US en_US.UTF-8
    sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
    export LANG=en_US.UTF-8

## Setup your keys
Next, you will download the key from the keyserver using the following command:

    sudo apt update && sudo apt install curl gnupg2 lsb-release
    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -

And then add it to your sources list:

    sudo sh -c 'echo "deb [arch=amd64,arm64] http://packages.ros.org/ros2/ubuntu `lsb_release -cs` main" > /etc/apt/sources.list.d/ros2-latest.list'

## Install ROS 2 packages
Update your apt repository so that your packages list is up to date.

    sudo apt update

### Install ROS2 Desktop version, which is the recommended one.

    sudo apt install ros-eloquent-desktop

*The above steps have been taken from the official ROS2 Installation Guide:  https://index.ros.org/doc/ros2/Installation/Dashing/Linux-Install-Debians/ *

## Use ROS2 off-the-shelf

Alternatively, you can use ROS2 Dashing off-the-shell in ROSDS, by clicking on the button below:

<a href="http://rosds.online/" target="_blank"><img src="../img/rosject.png" width="35%"></a>

*Above: Get ROSDS ROSject*

The link above will take yo a page like the below one:

<img src="../img/ros2-empty-rosject.png">

Now, you just need to Sign In (or Sign Up if you don't have an account yet) to ROSDS in order to launch the ROSject.

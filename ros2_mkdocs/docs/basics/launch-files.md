# Launch Files

We've seen that ROS2 can use launch files to execute programs. But... how do they work? Let's have a look.
Example 1.5

In Example 1.1, you used the command ros2 run in order to start the teleop_twist_keyboard node. But you've also seen that you can start nodes by using what we know as launch files.
But... how do they work? Let's have a look at an example. For instance, if we wanted to start the teleop_twist_keyboard node using a launch file, we would have to create something similar to the following Python script.
teleop_twist_keyboard.launch.py
In [ ]:

"""Launch a talker and a listener."""
​
​from launch import LaunchDescription
​import launch_ros.actions
​
​
​def generate_launch_description():
​return LaunchDescription([
​launch_ros.actions.Node(
​package='teleop_twist_keyboard', node_executable='teleop_twist_keyboard', output='screen'),
​])
​As you can see, the launch file structure is quite simple. First, we import some modules from the launch and launch_ros packages.
​In [ ]:
​
​from launch import LaunchDescription
​import launch_ros.actions
​Next, we define a function that will return a LaunchDescription object.
​In [ ]:
​
​def generate_launch_description():
​return LaunchDescription([
​launch_ros.actions.Node(
​package='teleop_twist_keyboard', node_executable='teleop_twist_keyboard', output='screen'),
​])
​Within the LaunchDescription object, we generate a Node where we will fill up the following parameters:
​package='package_name' # Name of the package that contains the code of the ROS program to execute
​node_executable='cpp_executable_name' # Name of the cpp executable file that we want to execute
​output='type_of_output' # Through which channel you will print the output of the program

## Get a ROSject

Alternatively, you can use ROS2 Dashing off-the-shell in ROSDS, by clicking on the button below:

[<img src="img/rosject.png">](http://rosds.online/)
![Screenshot](img/rosject.png)

*Above: Get ROSDS ROSject*


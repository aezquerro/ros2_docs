# Compile a package

## Create your package

First of all, make sure you are in the src folder of your workspace before running the package creation command.

    cd ros2_ws/src

Also, make sure that you have sourced your ROS2 installation.

    source opt/ros/dashing/setup.bash

Now let's create a couple of new packages, executing the following commands.

    ros2 pkg create a_package
    ros2 pkg create b_package

At the end, you should end up with the following 2 packages in your workspace:

<img src="../img/packages-ab.png">

For this example. we are not going to worry too much about how we create these package, since it is only for testing purposes.

## Compile your package
Return to the root of your workspace:

    cd ~/ros2_ws

Now you can build your packages:

    colcon build

You will get something like this in your shell:

<img src="../img/compile-workspace.png">

As you can see, this command compiles your whole src directory (`a_package` and `b_package`). Also, it is very important to state that **IT NEEDS** to be issued in the root of your workspace (for this tutorials it's `~/ros2_ws`). This is MANDATORY.

Sometimes (for example, in large projects) you will not want to compile all of your packages, but just the one(s) where you've made changes. You can do this with the following command:

    colcon build --packages-select a_package
    
Now, the output of the shell will indicate the compilation of **ONLY** the package specified in the command:

<img src="../img/compile-a-package.png">

## Source your workspace
Finally, remember yo source your workspace after building it.

    cd ~/ros2_ws
    source install/setup.bash

## Practice Online

Also, you can test this tutorial in ROSDS, using a ROSject which already contains all the code described in it. You can get the ROSject by clicking on the button below:

<a href="http://www.rosject.io/l/e111f62/" target="_blank"><img src="../img/rosject.png" width="35%"></a>

*Above: Get ROSDS ROSject*

The link above will take yo a page like the below one:

<img src="../img/ros2-empty-rosject.png">

Now, you just need to Sign In (or Sign Up if you don't have an account yet) to ROSDS in order to launch the ROSject.


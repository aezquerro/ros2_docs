# Create a new Workspace

## Create folder structure
Best practice is to create a new directory for every new workspace. The name doesn’t matter, but it is helpful to have it indicate the purpose of the workspace. For this one, let's call it `ros2_ws`:

    mkdir -p ~/ros2_ws/src

As you can see, we have also created an `src` folder inside the workspace. This folder is where you will place all the packages that you create.

## Build the workspace

From the root of your workspace `(~/ros2_ws)`, you can now build your packages using the command:`

    cd ~/ros2_ws
    colcon build

Once the build is finished, execute `ls` in the workspace root and you will see that colcon has created new directories:

    build  install  log  src

The install directory is where your workspace’s setup files are, which you can use to source your workspace.

## Source your worspace

Before sourcing your workspace, it is very important that you open a new terminal, separate from the one where you built the workspace. Sourcing a workspace in the same terminal where you built may create complex issues.

In the new terminal, source your main ROS2 environment as the “underlay”, so you can build the overlay “on top of” it:

    source /opt/ros/eloquent/setup.bash

(This is step is not necessary if you already have this line in your `.bashrc` file).

Next, go into the root of your workspace:

    cd ~/ros2_ws

In the root, source your workspace:   

    source install/local_setup.bash

Now you will be able to use all the packages of this workspace. Although it's empty currently, you will create various packages during this tutorials.

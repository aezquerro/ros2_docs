# Set Up your Environment

## Install argcomplete
ROS 2 command line tools use argcomplete to autocompletion. So if you want autocompletion, installing argcomplete is necessary.

    sudo apt install python3-argcomplete

## Source the setup script
Set up your environment by sourcing the following file.

    source /opt/ros/eloquent/setup.bash

In order to avoid having to execute this every time you open a new shell, it is recommended to add this line to your `.bashrc` file. You can do so with the following command:

    echo "source /opt/ros/eloquent/setup.bash" >> ~/.bashrc

## Install ROS 1 Bridge
The ros1_bridge will provide you compatibility with ROS1 nodes, so it is recommended to be installed.

Execute the following command to install it:

    sudo apt update
    sudo apt install ros-eloquent-ros1-bridge

You can check the ROS1 Bridge section of this documentation in order to see how to setp up the ROS1 Bridge.

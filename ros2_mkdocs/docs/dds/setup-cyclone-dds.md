# Set Up Cyclone DDS

In order to activate a different DDS implementation, you will need to set up the `RMW_IMPLEMENTATION` environment variable. For instance, let's execute the following steps:

## Check current RMW implementation (only for Eloquent)
In order to check the current RMW implementation we are using, we will use the `ros2 doctor` cli. This tool has been introduced on ROS2 Eloquent, and it is very useful for this use case.

First of all, let's source ROS2 Eloquent to make sure that we can have access the ros2 doctor tool.

    source /opt/ros/eloquent/setup.bash

Now, let's switch to a different  rmw implementation by specifying the environment variable.

    export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp

To make sure that you are using Cyclonde DDS, you can use the ros2doctor tool, with the following command:

    ros2 doctor --report
    
Among the report data, you will see a section that indicates the current RMW MIDDLEWARE used:

<img src="../img/ros2doctor-report.png">

## Testing the performance
Now that Cyclone DDS has successfully been activated, let's run some performance tests. For this, we will use iRobot’s ROS 2 performance tests: https://github.com/irobot-ros/ros2-performance.

In order to run the tests, let's open a new Shell. In the new Shell, let's first source the ROS2 version we want to use:

    source /opt/ros/eloquent/setup.bash

Now, let's make sure that our ROS2 workspace is properly compiled.

    cd ~/ros2_ws/
    colcon build

The compilation should finish without any error:

<img src="../img/colcon-build.png">

And let's source the workspace:

    source ~/ros2_ws/install/local_setup.bash

Finally, let's move to the following path:

    cd ~/ros2_ws/install/benchmark/lib/benchmark

And run the tests:

    ./benchmark topology/sierra_nevada.json

You will see the results of the tests printed to the Shell's output.

<img src="../img/benchmark-results.gif">

And also, the results will be saved to the following folder:

    ~/ros2_ws/install/benchmark/lib/benchmark/sierra_nevada_log/

You can check there the different files generated:

    cd ~/ros2_ws/install/benchmark/lib/benchmark/sierra_nevada_log
    ls
    
<img src="../img/benchmark-files.png">

## Practice Online

Also, you can test this tutorial in ROSDS, using a ROSject which already contains all the code described in it. You can get the ROSject by clicking on the button below:

<a href="http://www.rosject.io/l/e102032/" target="_blank"><img src="../img/rosject.png" width="35%"></a>

*Above: Get ROSDS ROSject*

The link above will take yo a page like the below one:

<img src="../img/rosject-cyclone.png">

Now, you just need to Sign In (or Sign Up if you don't have an account yet) to ROSDS in order to launch the ROSject.


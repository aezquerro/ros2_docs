# Installing additional DDS implementations

ROS2 installs, by default, the FastRTPS DDS implementation from eProsima. It is also set as the default implementation used. However, there's the possiblity to install different implementations and use them, of course.

Below you can see how to install different DDS implementations.

## PrismTech OpenSplice (not available for Eloquent at the moment)

    sudo apt install libopensplice69

## RTI Connext (not available for Eloquent at the moment)

You can install a Debian package of RTI Connext available on the ROS 2 apt repositories. You will need to accept a license from RTI.

    sudo apt install -q -y rti-connext-dds-5.3.1

Source the setup file to set the NDDSHOME environment variable.

    cd /opt/rti.com/rti_connext_dds-5.3.1/resource/scripts && source ./rtisetenv_x64Linux3gcc5.4.0.bash; cd -

Note: when using zsh you need to be in the directory of the script when sourcing it to have it work properly

Now you can build as normal and support for RTI will be built as well.

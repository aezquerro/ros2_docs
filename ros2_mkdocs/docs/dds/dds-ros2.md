# DDS on ROS2

## What is DDS?

The ROS client library defines an API which exposes communication concepts like publish / subscribe to users.

In ROS 1 the implementation of these communication concepts was built on custom protocols (e.g., TCPROS).

For ROS 2 the decision has been made to build it on top of an existing middleware solution (namely DDS). The major advantage of this approach is that ROS 2 can leverage an existing and well developed implementation of that standard.

ROS could build on top of one specific implementation of DDS. But there are numerous different implementations available and each has its own pros and cons in terms of supported platforms, programming languages, performance characteristics, memory footprint, dependencies and licensing.

Therefore ROS aims to support multiple DDS implementations despite the fact that each of them differ slightly in their exact API. In order to abstract from the specifics of these APIs, an abstract interface is being introduced which can be implemented for different DDS implementations. This middleware interface defines the API between the ROS client library and any specific implementation.

Each implementation of the interface will usually be a thin adapter which maps the generic middleware interface to the specific API of the middleware implementation. In the following the common separation of the adapter and the actual middleware implementation will be omitted.
